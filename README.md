# ECS

The entity component system

```Cpp
struct Physics
{
	float x, y;
	float vx, vy;
};

//start the multiverse
//multiverse starts with a single universe in it
Multiverse v = multiverse_new();

//start of universe 1
Universe u = multiverse_top(v);

//init 10 entities
for(size_t i = 0; i < 10; ++i)
{
	Entity e = universe_entity_new(u);
	Physics* p = universe_component_write<Physics>(u, e);
	p->x = p->y = i;
	p->vx = 1.0f;
	p->vy = -1.0f;
}

//multiverse forks into universe 2
u = multiverse_fork(v);

//invoke an update over them
size_t runs = universe_run<Physics>(u, [](Universe u, Entity e) {
	Physics* p = universe_component_write<Physics>(u, e);
	p->x += p->vx;
	p->y += p->vy;
});

CHECK(runs == 10);

//check that the state is updated
float i = 0.0f;
runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
	const Physics* p = universe_component_read<Physics>(u, e);
	CHECK(p->x == i + 1);
	CHECK(p->y == i - 1);
	++i;
});

CHECK(runs == 10);

//multiverse undo to universe 1
u = multiverse_undo(v);

//check that the state is back to where it was in universe 1
i = 0.0f;
runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
	const Physics* p = universe_component_read<Physics>(u, e);
	CHECK(p->x == i);
	CHECK(p->y == i);
	++i;
});

CHECK(runs == 10);

//multiverse redo to universe 2
u = multiverse_redo(v);

//check that the state is back to where it was in universe 2
i = 0.0f;
runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
	const Physics* p = universe_component_read<Physics>(u, e);
	CHECK(p->x == i + 1);
	CHECK(p->y == i - 1);
	++i;
});

CHECK(runs == 10);

//free the multiverse
multiverse_free(v);
```