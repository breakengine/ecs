#pragma once

#include <stdint.h>

namespace brk::ecs
{
	struct Entity
	{
		uint32_t id;
		//gen is always > 0, when gen == 0 this entity is invalid
		uint32_t gen;

		inline operator bool() const { return gen != 0; }

		inline bool
		operator==(const Entity& other) const
		{
			return id == other.id && gen == other.gen;
		}

		inline bool
		operator!=(const Entity& other) const
		{
			return !operator==(other);
		}
	};
}