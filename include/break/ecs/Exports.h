#pragma once

#if defined(OS_WINDOWS)
	#if defined(ECS_DLL)
		#define API_ECS __declspec(dllexport)
	#else
		#define API_ECS __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_ECS 
#endif