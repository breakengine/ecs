#pragma once

#include "break/ecs/Universe.h"
#include "break/ecs/Storage.h"

#include <mn/Buf.h>

namespace brk::ecs
{
	struct Multiverse
	{
		Storage storage;
		mn::Buf<Universe> stack;
		size_t index;
	};

	inline static Multiverse
	multiverse_new()
	{
		Multiverse self{};
		self.storage = storage_new();
		self.stack = mn::buf_lit({ universe_new(self.storage) });
		return self;
	}

	inline static void
	multiverse_free(Multiverse& self)
	{
		destruct(self.stack);
		storage_free(self.storage);
	}

	inline static void
	destruct(Multiverse& self)
	{
		multiverse_free(self);
	}

	inline static Universe
	multiverse_top(Multiverse& self)
	{
		return self.stack[self.index];
	}

	inline static Universe
	multiverse_fork(Multiverse& self)
	{
		for(size_t i = self.index + 1; i < self.stack.count; ++i)
			universe_free(self.stack[i]);
		self.stack.count = self.index + 1;
		Universe new_universe = universe_fork(mn::buf_top(self.stack));
		mn::buf_push(self.stack, new_universe);
		++self.index;
		return new_universe;
	}

	inline static Universe
	multiverse_undo(Multiverse& self)
	{
		if(self.index > 0)
			--self.index;
		return multiverse_top(self);
	}

	inline static Universe
	multiverse_redo(Multiverse& self)
	{
		if(self.index + 1 < self.stack.count)
			++self.index;
		return multiverse_top(self);
	}
}
