#pragma once

#include "break/ecs/Exports.h"
#include "break/ecs/Type_Table.h"

#include <mn/Base.h>

namespace brk::ecs
{
	MS_HANDLE(Storage);

	API_ECS Storage
	storage_new();

	API_ECS void
	storage_free(Storage storage);

	inline static void
	destruct(Storage storage)
	{
		storage_free(storage);
	}

	API_ECS void*
	storage_component_new(Storage storage, Type_ID id);

	API_ECS void
	storage_component_free(Storage storage, Type_ID id, void* ptr);

	API_ECS int
	component_refcount(void* component);

	API_ECS void
	component_ref(void* component);

	API_ECS void
	component_unref(void* component);


	//sugar coating
	template<typename T>
	inline static T*
	storage_component_new(Storage storage)
	{
		return (T*)storage_component_new(storage, type_id<T>());
	}

	template<typename T>
	inline static void
	storage_component_free(Storage storage, T* ptr)
	{
		storage_component_free(storage, type_id<T>(), ptr);
	}
}