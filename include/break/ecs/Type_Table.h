#pragma once

#include "break/ecs/Exports.h"

#include <typeinfo>
#include <stdint.h>
#include <stddef.h>

namespace brk::ecs
{
	using Type_ID = uint32_t;

	API_ECS Type_ID
	type_id(const std::type_info& info, size_t typesize);

	API_ECS size_t
	type_size(Type_ID id);

	API_ECS void
	type_set_clone(Type_ID id, void(*func)(void* dst, const void* src));

	API_ECS void
	type_clone(Type_ID id, void* dst, const void* src);

	API_ECS void
	type_set_free(Type_ID id, void(*func)(void* self));

	API_ECS void
	type_free(Type_ID id, void* self);

	//sugar coating
	template<typename T>
	inline static Type_ID
	type_id()
	{
		return type_id(typeid(T), sizeof(T));
	}
}