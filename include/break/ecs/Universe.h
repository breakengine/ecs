#pragma once

#include "break/ecs/Exports.h"
#include "break/ecs/Entity.h"
#include "break/ecs/Type_Table.h"

#include <mn/Base.h>

namespace brk::ecs
{
	MS_HANDLE(Universe);

	MS_FWD_HANDLE(Storage);

	API_ECS Universe
	universe_new(Storage storage);

	API_ECS void
	universe_free(Universe universe);

	inline static void
	destruct(Universe universe)
	{
		universe_free(universe);
	}

	API_ECS Universe
	universe_fork(Universe other);

	//entity interface
	API_ECS Entity
	universe_entity_new(Universe universe);

	API_ECS void
	universe_entity_free(Universe universe, Entity entity);

	API_ECS bool
	universe_entity_exists(Universe universe, Entity entity);


	//component interface
	API_ECS void*
	universe_component_write(Universe universe, Entity entity, Type_ID id);

	API_ECS const void*
	universe_component_read(Universe universe, Entity entity, Type_ID id);


	//system interface
	struct ISystem
	{
		virtual void run(Universe universe, Entity entity) = 0;
	};

	API_ECS size_t
	universe_system_run(Universe universe, ISystem* system, Type_ID id);


	//sugar coating
	template<typename T>
	inline static T*
	universe_component_write(Universe universe, Entity entity)
	{
		return (T*)universe_component_write(universe, entity, type_id<T>());
	}

	template<typename T>
	inline static const T*
	universe_component_read(Universe universe, Entity entity)
	{
		return (const T*)universe_component_read(universe, entity, type_id<T>());
	}

	template<typename T>
	inline static size_t
	universe_system_run(Universe universe, ISystem* system)
	{
		return universe_system_run(universe, system, type_id<T>());
	}

	template<typename TLambda>
	struct Lambda_System final : ISystem
	{
		TLambda lambda;

		Lambda_System(TLambda func)
			:lambda(func)
		{}

		void run(Universe universe, Entity entity) override
		{
			lambda(universe, entity);
		}
	};

	template<typename T, typename TLambda>
	inline static size_t
	universe_run(Universe universe, TLambda&& lambda)
	{
		Lambda_System<TLambda> system(lambda);
		return universe_system_run(universe, &system, type_id<T>());
	}


}