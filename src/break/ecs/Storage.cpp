#include "break/ecs/Storage.h"

#include <mn/Buf.h>
#include <mn/Pool.h>
#include <mn/Thread.h>

#include <atomic>
#include <stdint.h>

namespace brk::ecs
{
	using namespace mn;

	using Ref_Count = std::atomic<uint16_t>;

	struct IStorage
	{
		Buf<Pool> pools;
		Mutex pools_mtx;
	};


	//API
	Storage
	storage_new()
	{
		IStorage* self = alloc<IStorage>();
		self->pools = buf_new<Pool>();
		self->pools_mtx = mutex_new("Storage Mutex");
		return (Storage)self;
	}

	void
	storage_free(Storage storage)
	{
		IStorage* self = (IStorage*)storage;
		//free the pools
		for(Pool p:self->pools)
			if(p) pool_free(p);
		buf_free(self->pools);
		//free the mutex
		mutex_free(self->pools_mtx);
		//free the storage
		free(self);
	}

	void*
	storage_component_new(Storage storage, Type_ID id)
	{
		IStorage* self = (IStorage*)storage;

		mutex_lock(self->pools_mtx);

		//ensure the type pool exist
		if(id >= self->pools.count)
			buf_resize_fill(self->pools, id + 1, nullptr);

		size_t element_size = type_size(id);

		//ensure the pool is initialized
		if(self->pools[id] == nullptr)
			self->pools[id] = pool_new(sizeof(Ref_Count) + element_size, 1024);

		//we put the ref count in front of the component
		uint8_t* ptr = (uint8_t*)pool_get(self->pools[id]);

		mutex_unlock(self->pools_mtx);

		Ref_Count* rc = (Ref_Count*)ptr;
		//set the ref count to 1
		std::atomic_store(rc, uint16_t(1));

		//get the result ptr
		void* result = ptr + sizeof(Ref_Count);

		return result;
	}

	void
	storage_component_free(Storage storage, Type_ID id, void* component)
	{
		IStorage* self = (IStorage*)storage;

		uint8_t* ptr = (uint8_t*)component;
		Ref_Count* rc = (Ref_Count*)(ptr - sizeof(Ref_Count));

		//if this is the last reference
		uint16_t result = std::atomic_fetch_sub(rc, uint16_t(1));
		assert(result >= 1 && "maybe you called component_unref when you shouldn't");
		if(result == 1)
		{
			mutex_lock(self->pools_mtx);
			type_free(id, component);
			pool_put(self->pools[id], ptr);
			mutex_unlock(self->pools_mtx);
		}
	}

	int
	component_refcount(void* component)
	{
		uint8_t* ptr = (uint8_t*)component;
		Ref_Count* rc = (Ref_Count*)(ptr - sizeof(Ref_Count));
		return std::atomic_load(rc);
	}

	void
	component_ref(void* component)
	{
		uint8_t* ptr = (uint8_t*)component;
		Ref_Count* rc = (Ref_Count*)(ptr - sizeof(Ref_Count));
		std::atomic_fetch_add(rc, uint16_t(1));
	}

	void
	component_unref(void* component)
	{
		uint8_t* ptr = (uint8_t*)component;
		Ref_Count* rc = (Ref_Count*)(ptr - sizeof(Ref_Count));
		std::atomic_fetch_sub(rc, uint16_t(1));
	}
}
