#include "break/ecs/Type_Table.h"

#include <mn/Map.h>
#include <mn/Thread.h>

namespace brk::ecs
{
	using namespace mn;

	struct Type_Entry
	{
		size_t hashcode;
		size_t typesize;
		Type_ID id;
		void(*clone)(void* dst, const void* src);
		void(*free)(void* self);
	};

	//type table
	struct Type_Table
	{
		//type generation table
		Map<size_t, Type_ID> ids;
		Type_ID ids_counter;

		//other into on the types
		Map<Type_ID, Type_Entry> entries;

		//global lock on the table
		Mutex_RW mtx;
	};

	inline static Type_Table
	type_table_new()
	{
		Type_Table self{};
		self.ids = map_new<size_t, Type_ID>();
		self.entries = map_new<Type_ID, Type_Entry>();
		self.mtx = mutex_rw_new("Global Type Table Mutex");
		return self;
	}

	inline static void
	type_table_free(Type_Table& self)
	{
		map_free(self.ids);
		map_free(self.entries);
		mutex_rw_free(self.mtx);
	}


	//C++ wrapper to free the global variable when the library unloads
	struct Type_Table_Wrapper
	{
		Type_Table table;

		Type_Table_Wrapper()
		{
			table = type_table_new();
		}

		~Type_Table_Wrapper()
		{
			type_table_free(table);
		}
	};

	inline static Type_Table*
	_table()
	{
		static Type_Table_Wrapper wrapper;
		return &wrapper.table;
	}


	//API
	Type_ID
	type_id(const std::type_info& info, size_t typesize)
	{
		Type_Table* self = _table();

		Type_Entry entry{};
		entry.hashcode = info.hash_code();
		entry.typesize = typesize;

		mutex_write_lock(self->mtx);
		if(auto it = map_lookup(self->ids, entry.hashcode))
		{
			//the type already exists so we set the entry to the stored entry data
			entry = map_lookup(self->entries, it->value)->value;
		}
		else
		{
			//type doesn't exists so we generate an id and add it
			entry.id = self->ids_counter++;
			map_insert(self->ids, entry.hashcode, entry.id);
			map_insert(self->entries, entry.id, entry);
		}
		mutex_write_unlock(self->mtx);

		return entry.id;
	}

	size_t
	type_size(Type_ID id)
	{
		Type_Table* self = _table();

		size_t result = 0;

		mutex_read_lock(self->mtx);
		if(auto it = map_lookup(self->entries, id))
			result = it->value.typesize;
		mutex_read_unlock(self->mtx);

		return result;
	}

	void
	type_set_clone(Type_ID id, void(*func)(void* dst, const void* src))
	{
		Type_Table* self = _table();

		mutex_write_lock(self->mtx);
		auto it = map_lookup(self->entries, id);
		it->value.clone = func;
		mutex_write_unlock(self->mtx);
	}

	void
	type_clone(Type_ID id, void* dst, const void* src)
	{
		Type_Table* self = _table();

		mutex_read_lock(self->mtx);
		auto it = map_lookup(self->entries, id);
		if(it->value.clone)
			it->value.clone(dst, src);
		else
			::memcpy(dst, src, it->value.typesize);
		mutex_read_unlock(self->mtx);
	}

	void
	type_set_clone(Type_ID id, void(*func)(void* self))
	{
		Type_Table* self = _table();

		mutex_write_lock(self->mtx);
		auto it = map_lookup(self->entries, id);
		it->value.free = func;
		mutex_write_unlock(self->mtx);
	}

	void
	type_free(Type_ID id, void* obj)
	{
		Type_Table* self = _table();

		mutex_read_lock(self->mtx);
		auto it = map_lookup(self->entries, id);
		if(it->value.free)
			it->value.free(obj);
		mutex_read_unlock(self->mtx);
	}
}