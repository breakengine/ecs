#include "break/ecs/Universe.h"
#include "break/ecs/Storage.h"

#include <mn/Buf.h>
#include <mn/Thread.h>

namespace brk::ecs
{
	using namespace mn;

	constexpr static uint32_t INVALID_ENTITY_HEAD = 0xFFFFFFFF;

	struct IComponent
	{
		Entity entity;
		void* component;
	};

	struct IUniverse
	{
		Storage storage;

		//entites bag
		Buf<Entity> entites;
		uint32_t	entites_head;

		//component table[type id][entity id]
		Buf<Buf<IComponent>> components;

		Mutex_RW mtx;
	};

	inline static bool
	_universe_component_exists(IUniverse* self, Entity entity, Type_ID id)
	{
		return (id < self->components.count &&
				entity.id < self->components[id].count &&
				entity == self->components[id][entity.id].entity);
	}


	//API
	Universe
	universe_new(Storage storage)
	{
		IUniverse* self = alloc<IUniverse>();
		self->storage = storage;
		self->entites = buf_new<Entity>();
		self->entites_head = INVALID_ENTITY_HEAD;
		self->components = buf_new<Buf<IComponent>>();
		self->mtx = mutex_rw_new("Universe Mutex");
		return (Universe)self;
	}

	void
	universe_free(Universe universe)
	{
		IUniverse* self = (IUniverse*)universe;
		buf_free(self->entites);

		for(size_t i = 0; i < self->components.count; ++i)
		{
			auto& type_bucket = self->components[i];
			for(const auto& entry : type_bucket)
				storage_component_free(self->storage, i, entry.component);
		}
		free(self);
	}

	Universe
	universe_fork(Universe other_universe)
	{
		IUniverse* self = alloc<IUniverse>();
		IUniverse* other = (IUniverse*)other_universe;

		self->storage = other->storage;
		self->entites = clone(other->entites);
		self->entites_head = other->entites_head;

		//increment the ref count
		for(const auto& type_bucket : other->components)
		{
			for(const auto& entry : type_bucket)
			{
				if(entry.entity)
				{
					component_ref(entry.component);
				}
			}
		}

		self->components = clone(other->components);
		self->mtx = mutex_rw_new("Universe Mutex");
		return (Universe)self;
	}

	Entity
	universe_entity_new(Universe universe)
	{
		IUniverse* self = (IUniverse*)universe;

		Entity entity;

		mutex_write_lock(self->mtx);
		if(self->entites_head == INVALID_ENTITY_HEAD)
		{
			entity.id = self->entites.count;
			entity.gen = 1;
			buf_push(self->entites, entity);
		}
		else
		{
			//we swap the head entity with the head next
			//this is simply a linked list
			entity = self->entites[self->entites_head];
			uint32_t id = self->entites_head;
			self->entites_head = entity.id;
			entity.id = id;
		}
		mutex_write_unlock(self->mtx);

		return entity;
	}

	void
	universe_entity_free(Universe universe, Entity entity)
	{
		IUniverse* self = (IUniverse*)universe;

		mutex_write_lock(self->mtx);

		assert(entity.id < self->entites.count && "Entity doesn't belong to this universe");

		//linked list push front logic
		//remember that this is a linked list
		self->entites[entity.id].id = self->entites_head;
		self->entites[entity.id].gen++;
		self->entites_head = entity.id;

		mutex_write_unlock(self->mtx);
	}

	bool
	universe_entity_exists(Universe universe, Entity entity)
	{
		IUniverse* self = (IUniverse*)universe;
		if(entity.id > self->entites.count)
			return false;
		//entities should be of the same generation to be equal
		//this check is to avoid use after free
		return self->entites[entity.id].gen == entity.gen;
	}

	void*
	universe_component_write(Universe universe, Entity entity, Type_ID id)
	{
		IUniverse* self = (IUniverse*)universe;

		//entity must exist in the universe
		assert(universe_entity_exists(universe, entity));

		void* result = nullptr;

		mutex_write_lock(self->mtx);

		//component doesn't exists let's create it
		if(_universe_component_exists(self, entity, id) == false)
		{
			result = storage_component_new(self->storage, id);

			if(id >= self->components.count)
			{
				//ensure that the components table is wide enough
				buf_resize_fill(self->components, id + 1, buf_new<IComponent>());
			}

			if(entity.id >= self->components[id].count)
			{
				//ensure that the components table is big enough
				buf_resize_fill(self->components[id], entity.id + 1, IComponent{});
			}

			self->components[id][entity.id] = IComponent{ entity, result };
		}
		//component already exists
		else
		{
			IComponent& entry = self->components[id][entity.id];
			//check the ref count if it exists in this universe alone then no need to copy
			//if it exists in multiple universes then make a copy
			if(component_refcount(entry.component) > 1)
			{
				void* copy = storage_component_new(self->storage, id);
				type_clone(id, copy, entry.component);
				//free the old
				storage_component_free(self->storage, id, entry.component);
				entry.component = copy;
			}

			result = entry.component;
		}

		mutex_write_unlock(self->mtx);

		return result;
	}

	const void*
	universe_component_read(Universe universe, Entity entity, Type_ID id)
	{
		IUniverse* self = (IUniverse*)universe;

		//entity must exist in the universe
		assert(universe_entity_exists(universe, entity));

		const void* result = nullptr;

		mutex_read_lock(self->mtx);

		if(_universe_component_exists(self, entity, id))
			result = self->components[id][entity.id].component;

		mutex_read_unlock(self->mtx);

		return result;
	}

	size_t
	universe_system_run(Universe universe, ISystem* system, Type_ID id)
	{
		IUniverse* self = (IUniverse*)universe;

		Buf<IComponent> entries = buf_new<IComponent>();

		mutex_read_lock(self->mtx);
		for(const auto& entry : self->components[id])
			if(entry.component != nullptr)
				buf_push(entries, entry);
		mutex_read_unlock(self->mtx);

		size_t result = entries.count;

		for(const IComponent& entry : entries)
			system->run(universe, entry.entity);

		buf_free(entries);
		return result;
	}
}
