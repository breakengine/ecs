#include <catch2/catch.hpp>

#include <break/ecs/Type_Table.h>
#include <break/ecs/Storage.h>
#include <break/ecs/Universe.h>
#include <break/ecs/Multiverse.h>

using namespace brk::ecs;

TEST_CASE("Type Table", "[ecs]")
{
	SECTION("Case 01")
	{
		Type_ID int_id = type_id<int>();
		Type_ID float_id = type_id<float>();
		CHECK(int_id != float_id);
		CHECK(int_id == type_id<int>());
		CHECK(float_id == type_id<float>());
		CHECK(int_id == type_id<const int>());
		CHECK(int_id != type_id<int*>());
	}
}

TEST_CASE("Storage", "[ecs]")
{
	SECTION("Case 01")
	{
		Storage s = storage_new();

		int* num = storage_component_new<int>(s);
		*num = 10;
		CHECK(*num == 10);
		CHECK(component_refcount(num) == 1);

		component_ref(num);
		CHECK(component_refcount(num) == 2);

		storage_component_free(s, num);

		CHECK(component_refcount(num) == 1);

		storage_component_free(s, num);

		storage_free(s);
	}
}

TEST_CASE("Universe", "[ecs]")
{
	SECTION("Case 01")
	{
		Storage s = storage_new();
		Universe u = universe_new(s);

		Entity a = universe_entity_new(u);

		CHECK(a == true);

		CHECK(universe_entity_exists(u, a) == true);

		universe_entity_free(u, a);

		CHECK(universe_entity_exists(u, a) == false);

		universe_free(u);
		storage_free(s);
	}
}

struct Physics
{
	float x, y;
	float vx, vy;
};

TEST_CASE("Systems", "[ecs]")
{
	SECTION("Case 01")
	{
		Storage s = storage_new();
		Universe u = universe_new(s);

		//init 10 entities
		for(size_t i = 0; i < 10; ++i)
		{
			Entity e = universe_entity_new(u);
			Physics* p = universe_component_write<Physics>(u, e);
			p->x = p->y = i;
			p->vx = 1.0f;
			p->vy = -1.0f;
		}

		//invoke an update over them
		size_t runs = universe_run<Physics>(u, [](Universe u, Entity e) {
			Physics* p = universe_component_write<Physics>(u, e);
			p->x += p->vx;
			p->y += p->vy;
		});

		CHECK(runs == 10);

		float i = 0.0f;
		runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
			const Physics* p = universe_component_read<Physics>(u, e);
			CHECK(p->x == i + 1);
			CHECK(p->y == i - 1);
			++i;
		});

		CHECK(runs == 10);

		universe_free(u);
		storage_free(s);
	}
}

TEST_CASE("Multiverse", "[ecs]")
{
	SECTION("Case 01")
	{
		//start the multiverse
		//multiverse starts with a single universe in it
		Multiverse v = multiverse_new();

		//start of universe 1
		Universe u = multiverse_top(v);

		//init 10 entities
		for(size_t i = 0; i < 10; ++i)
		{
			Entity e = universe_entity_new(u);
			Physics* p = universe_component_write<Physics>(u, e);
			p->x = p->y = i;
			p->vx = 1.0f;
			p->vy = -1.0f;
		}

		//multiverse forks into universe 2
		u = multiverse_fork(v);

		//invoke an update over them
		size_t runs = universe_run<Physics>(u, [](Universe u, Entity e) {
			Physics* p = universe_component_write<Physics>(u, e);
			p->x += p->vx;
			p->y += p->vy;
		});

		CHECK(runs == 10);

		//check that the state is updated
		float i = 0.0f;
		runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
			const Physics* p = universe_component_read<Physics>(u, e);
			CHECK(p->x == i + 1);
			CHECK(p->y == i - 1);
			++i;
		});

		CHECK(runs == 10);

		//multiverse undo to universe 1
		u = multiverse_undo(v);

		//check that the state is back to where it was in universe 1
		i = 0.0f;
		runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
			const Physics* p = universe_component_read<Physics>(u, e);
			CHECK(p->x == i);
			CHECK(p->y == i);
			++i;
		});

		CHECK(runs == 10);

		//multiverse redo to universe 2
		u = multiverse_redo(v);

		//check that the state is back to where it was in universe 2
		i = 0.0f;
		runs = universe_run<Physics>(u, [&](Universe u, Entity e) {
			const Physics* p = universe_component_read<Physics>(u, e);
			CHECK(p->x == i + 1);
			CHECK(p->y == i - 1);
			++i;
		});

		CHECK(runs == 10);

		//free the multiverse
		multiverse_free(v);
	}
}
